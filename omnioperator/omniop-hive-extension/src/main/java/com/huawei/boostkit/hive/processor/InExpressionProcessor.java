/*
 * Copyright (C) 2023-2024. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.boostkit.hive.processor;

import static com.huawei.boostkit.hive.expression.ExpressionUtils.getExprNodeColumnDesc;

import com.huawei.boostkit.hive.expression.BaseExpression;
import com.huawei.boostkit.hive.expression.ExpressionUtils;
import com.huawei.boostkit.hive.expression.TypeUtils;
import com.huawei.boostkit.hive.expression.UnaryExpression;

import org.apache.hadoop.hive.ql.plan.ExprNodeColumnDesc;
import org.apache.hadoop.hive.ql.plan.ExprNodeConstantDesc;
import org.apache.hadoop.hive.ql.plan.ExprNodeDesc;
import org.apache.hadoop.hive.ql.plan.ExprNodeGenericFuncDesc;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;

import java.util.List;

public class InExpressionProcessor implements ExpressionProcessor {
    @Override
    public BaseExpression process(ExprNodeGenericFuncDesc node, String operator, ObjectInspector inspector) {
        List<ExprNodeDesc> children = node.getChildren();
        UnaryExpression in = new UnaryExpression("IN", TypeUtils.convertHiveTypeToOmniType(node.getTypeInfo()),
                children.size());
        TypeInfo nodeColumnTypeInfo = getExprNodeColumnDesc(node).get(0).getTypeInfo();
        for (ExprNodeDesc child : children) {
            if (child instanceof ExprNodeGenericFuncDesc) {
                in.add(ExpressionUtils.build((ExprNodeGenericFuncDesc) child, inspector));
            } else if (child instanceof ExprNodeColumnDesc) {
                in.add(ExpressionUtils.createReferenceNode(child, inspector));
            } else if (child instanceof ExprNodeConstantDesc) {
                if (!child.getTypeInfo().equals(nodeColumnTypeInfo)) {
                    child = new ExprNodeConstantDesc(nodeColumnTypeInfo, ((ExprNodeConstantDesc) child).getValue());
                }
                in.add(ExpressionUtils.createLiteralNode(child));
            }
        }
        return in;
    }
}
