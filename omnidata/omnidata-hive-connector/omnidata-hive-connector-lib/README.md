# OmniData Hive Connector Lib

## Building OmniData Hive Connector Lib

1. Simply run the following command from the project root directory:<br>
`mvn clean package`<br>
Then you will find jars in the "omnidata-hive-connector-lib/target/" directory.

## More Information

For further assistance, send an email to kunpengcompute@huawei.com.